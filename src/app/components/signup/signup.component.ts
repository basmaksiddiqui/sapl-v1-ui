import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"],
})
export class SignupComponent implements OnInit {
  signupContentValidations: FormGroup;
  user:User;

  matcher = new MyErrorStateMatcher();

  constructor(private userApiService: UserService,
    private formBuilder: FormBuilder,
    private router: Router) {
    
  }

  ngOnInit() {
    // Validation check for signup fields
    this.signupContentValidations = this.formBuilder.group({
      email: ["", [Validators.email, Validators.required]],
      username: ["", [Validators.required, Validators.minLength(2)]],
      name: ["", [Validators.required, Validators.minLength(2)]],
      password: ["", [Validators.required, Validators.minLength(4)]]
    });
  }

  onSignupClicked() {
    this.user = this.signupContentValidations.value;
    // generate random user id.
    this.user.id = '_' + Math.random().toString(36).substr(2, 9);
    
    console.log("Signup clicked!!" + JSON.stringify(this.user ));
    this.userApiService
      .doSignup(this.user)
      .subscribe((user) => {
        if (user.name == "Error Occured") {
          console.log("Error occured.");
        } else {
          console.log("signup component> signup Sucess : " + user);
          this.gotoLoginPage();
        }
      });
  }

  gotoLoginPage() {
    this.router.navigate(['/login']);
  }

  // Reset take note form
  private resetFormContent() {
    this.signupContentValidations.reset();
    this.signupContentValidations.markAsPristine();
    this.signupContentValidations.markAsUntouched();
  }
}
