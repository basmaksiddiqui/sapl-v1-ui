import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';

import {ErrorStateMatcher} from '@angular/material/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginContentValidations: FormGroup;

  
  emailFormControl = new FormControl('', [
    Validators.required
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(4)
  ]);

  matcher = new MyErrorStateMatcher();

  constructor(private userApiService: UserService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.loginContentValidations = this.formBuilder.group({
      username: ["", [Validators.required, Validators.minLength(1)]],
      password: ["", [Validators.required, Validators.minLength(1)]]
    });
  }

  onLoginClicked() {
    console.log("Login clicked!!");
    this.userApiService
      .doLogin(this.loginContentValidations.value)
      .subscribe((user) => {
        if (user.name == "Error Occured") {
          console.log("Error occured.");
          this.openSnackBar("Login Failed!! Try Agian", "");
        } else {
          console.log("Login component> Login Sucess : " + JSON.stringify(user ));
          this.openSnackBar("Login Success!! Going to the dashboard", "");
          this.gotoDashboard();
        }
      });
  }
  gotoDashboard() {
    this.router.navigate(['/dashboard']);
  }

  openSnackBar(message: string, action: string) {
    // this._snackBar.open(message, action, {
    //   duration: 2000,
    // });
  }

}
