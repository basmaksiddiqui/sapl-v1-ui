import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor(private router : Router) { }

  routeToLogin(){
    this.router.navigate['user/login']
  }

  routeToSignUp(){
    this.router.navigate['user/signup']
  }

  routeToUserDashboard(){
    this.router.navigate['user/dashboard']
  }
  routeToUserProfile(){
    this.router.navigate['user/profile']
  }
}
